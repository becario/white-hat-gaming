# Gaming

This is an exercise for 'White Hat Gaming' -> https://mxfinder.com/test

The exercise has been completed as requested. The time invested has been around 16/17 hours (2 days).
It has been tested in the following devices/browsers:
- iPhone6 (ios12) Safari
- Huawei P9 Lite (Android 7) Chrome
- Mac Chrome
- Windows7 Chrome

The approach has been to deliver the requirements concisely avoiding the usage of 3rd party libraries.

# Comments regarding the 'Task Priorities'

1. Responsive design has been done for different touch devices (until 768px width) and medium/big sized screens (from 769px).
2. Access to the 'production' url is: https://mxfinder.com/test Note that Apache's htaccess file has not been modified and inner urls of this test are not accessible directly.
3. ---
4. There is an interval of 3 seconds to refresh the Jackpot values.
5. I'm not sure to have understood that point correctly: Given featured categories, which are 'new' and 'top', show a ribbon to the game belonging to a featured category that is different of the current section category. Check GameGalleryComponent. The images for the ribbon (white and green) have been extracted from the sample design.
6. I tried to use the play ascii character ► but visual inconsistency across operating systems made that approach invalid, however the result is a pure CSS solution.
7. ---

# Bonus features

- Loading animation for initial data reception.
- Wave animation every time the app recives new Jackpot values.
- Hover animations (laptop and desktop)
- Animations for section change (I had some problems here building for production. Please checkout page-slide.ts)
- Responsive menu layout.
- Touch events for smartphones and tablets.

# Unit testing

2 methods have been unit tested with Karma + Jasmine and rxjs-marbles.
The problem I bumped yesterday was a bug while testing an app with ngrx 8: https://stackoverflow.com/questions/60672156/unable-to-override-mockstore

# Commit list from latest to first:

- 2020-04-28 00:00 +0200 Lluis Nieto o [FEAT] Successfully test a couple of GameGalleryService (sync and async).
- 2020-04-27 16:55 +0200 Lluis Nieto o [FIX] Readme file.
- 2020-04-27 15:51 +0200 Lluis Nieto o [FEAT] Request mocks added.
- 2020-04-27 00:21 +0200 Lluis Nieto o [FIX] Readme file update and signature.

- 2020-04-26 22:56 +0200 Lluis Nieto o [FIX] Unit test attempt of async events (redux effect) using jasmine marbles for Angular.
- 2020-04-26 21:19 +0200 Lluis Nieto o [FIX] Small UX tweaks.
- 2020-04-26 20:57 +0200 Lluis Nieto o [FEAT] Add touch events.
- 2020-04-26 19:44 +0200 Lluis Nieto o [FEAT] Loading animation for data downloading.
- 2020-04-26 15:47 +0200 Lluis Nieto o [FEAT] Mount jackpot ribbon and schedule interval for data refresh.
- 2020-04-26 11:42 +0200 Lluis Nieto o [FIX] Play button compatibility issues fix due to font usage.
- 2020-04-26 11:16 +0200 Lluis Nieto o [FIX] Tweaks to enable a working production build.
- 2020-04-26 00:39 +0200 Lluis Nieto o [FEAT] Play button on hover game.
- 2020-04-25 23:23 +0200 Lluis Nieto o [FEAT] Implement animations for section change.
- 2020-04-25 22:16 +0200 Lluis Nieto o [FEAT] Featured ribbon assignation.
- 2020-04-25 20:10 +0200 Lluis Nieto o [FEAT] Responsive menu.
- 2020-04-25 18:49 +0200 Lluis Nieto o [FIX] Game assignation per menu category.
- 2020-04-25 18:09 +0200 Lluis Nieto o [FEAT] Menu component + routes setup.
- 2020-04-25 15:10 +0200 Lluis Nieto o [FEAT] Game gallery component.
- 2020-04-25 12:24 +0200 Lluis Nieto o [FEAT] Jackpots redux feature.
- 2020-04-25 11:53 +0200 Lluis Nieto o [FEAT] Setup ngrx infrastructure + games redux feature.
- 2020-04-25 10:39 +0200 Lluis Nieto o [FIX] Remove new project sample code.
- 2020-04-25 10:35 +0200 Lluis Nieto I [FEAT] Fresh Angular installation.

Public repository available at: https://becario@bitbucket.org/becario/white-hat-gaming.git
