import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map, take } from 'rxjs/operators';

import { Game } from '../redux/games/games.state';
import { Jackpot } from '../redux/jackpots/jackpots.state';

@Injectable()
export class ApiService {

  constructor(private httpClient: HttpClient) {}

  public getGames(): Observable<Game[]> {
    const path = 'https://stage.whgstage.com/front-end-test/games.php';
    return this.httpClient.get<Game[]>(path).pipe(take(1));
  }

  public getJackpots(): Observable<Jackpot[]> {
    const path = 'https://stage.whgstage.com/front-end-test/jackpots.php';
    return this.httpClient.get<any[]>(path).pipe(
      take(1),
      map(this.castJackpotsResponse)
    );
  }

  private castJackpotsResponse = (jackpots: any[]) => {
    return jackpots.map((jackpot: any) => {
      jackpot.id = jackpot.game;
      jackpot.currency = 'GBP';
      delete jackpot.game;

      return jackpot as Jackpot;
    });
  };
}
