import { Route, Routes } from '@angular/router';
import { GameGalleryComponent } from './components/game-gallery/game-gallery.component';

export const routes: Routes = [
  {
    path: '',
    redirectTo: '/top-games',
    pathMatch: 'full'
  },
  {
    path: 'top-games',
    component: GameGalleryComponent,
    data: {
      label: 'Top games',
      categories: ['top'],
      animation: 'page0'
    }
  },
  {
    path: 'new-games',
    component: GameGalleryComponent,
    data: {
      label: 'New games',
      categories: ['new'],
      animation: 'page1'
    }
  },
  {
    path: 'slots',
    component: GameGalleryComponent,
    data: {
      label: 'Slots',
      categories: ['slots'],
      animation: 'page2'
    }
  },
  {
    path: 'jackpots',
    component: GameGalleryComponent,
    data: {
      label: 'Jackpots',
      animation: 'page3'
    }
  },
  {
    path: 'live',
    component: GameGalleryComponent,
    data: {
      label: 'Live',
      animation: 'page4'
    }
  },
  {
    path: 'blackjack',
    component: GameGalleryComponent,
    data: {
      label: 'Blackjack',
      categories: ['blackjack'],
      animation: 'page5'
    }
  },
  {
    path: 'roulette',
    component: GameGalleryComponent,
    data: {
      label: 'Roulette',
      categories: ['roulette'],
      animation: 'page6'
    }
  },
  {
    path: 'table',
    component: GameGalleryComponent,
    data: {
      label: 'Table',
      animation: 'page7'
    }
  },
  {
    path: 'poker',
    component: GameGalleryComponent,
    data: {
      label: 'Poker',
      categories: ['poker'],
      animation: 'page8'
    }
  },
  {
    path: 'other',
    component: GameGalleryComponent,
    data: {
      label: 'Other',
      categories: ['ball', 'virtual', 'fun'],
      animation: 'page9'
    }
  }
];

export interface GamingRoute extends Route {
  data: {
    label: string;
    categories: string[];
    animation?: string;
  }
}
export const optionRoutes = routes.filter((route) => !!route.component) as GamingRoute[];
