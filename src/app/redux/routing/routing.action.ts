import { createAction, props } from '@ngrx/store';
import { GamingRoute } from '../../routes';

export const actionRoutingAddPath = createAction(
  '[ROUTING] Add path',
  props<{ newRoute: GamingRoute }>()
);

export const actionChangeLoadingStatus = createAction(
  '[ROUTING] Change loading status',
  props<{ value: boolean }>()
);
