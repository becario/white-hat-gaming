import { createFeatureSelector } from '@ngrx/store';
import { GamingRoute } from '../../routes';

export interface RoutingState {
  routeHistory: GamingRoute[];
  routes: GamingRoute[];
  loading: boolean;
}
export interface SiblingRoutes {
  prev: GamingRoute;
  next: GamingRoute;
}

export const getRoutingModuleState = createFeatureSelector<RoutingState>(
  'routing'
);
