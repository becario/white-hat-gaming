import { createSelector } from '@ngrx/store';
import { getRoutingModuleState } from './routing.state';

export const selectorRoutingGetCurrentRoute = createSelector(
  getRoutingModuleState,
  state => !!state && !!state.routeHistory[0] && state.routeHistory[0]
);

export const selectorRoutingGetLoadingStatus = createSelector(
  getRoutingModuleState,
  state => state.loading
);

export const selectorRoutingGetAvailableRoutes = createSelector(
  getRoutingModuleState,
  state => !!state && state.routes
);
