import { Action, createReducer, on } from '@ngrx/store';

import { RoutingState } from './routing.state';
import { optionRoutes } from '../../routes';
import * as RoutingActions from './routing.action';

export const initialState: RoutingState = {
  routeHistory: [],
  routes: optionRoutes,
  loading: true
};
const reducer = createReducer(
  initialState,
  on(RoutingActions.actionRoutingAddPath, (state, payload) => {
    const newPathHistory = [payload.newRoute, ...state.routeHistory];
    return {
      ...state,
      routeHistory: newPathHistory
    }
  }),
  on(RoutingActions.actionChangeLoadingStatus, (state, payload) => {
    return {
      ...state,
      loading: payload.value
    }
  })
);

export function routingReducers(state: RoutingState | undefined, action: Action) {
  return reducer(state, action);
}
