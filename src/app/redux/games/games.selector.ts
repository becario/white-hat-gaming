import { createSelector } from '@ngrx/store';
import { getGamesModuleState } from './games.state';

export const selectorGamesGetAll = createSelector(
  getGamesModuleState,
  state => {
    const ids: any[] = state.ids;
    const games = ids.map((id) => state.entities[id]);

    return games;
  }
);
