import { TestBed, tick } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MemoizedSelector, MemoizedSelectorWithProps, Store, StoreModule } from '@ngrx/store';
import { RouterModule } from '@angular/router';
import { EffectsModule } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { fakeSchedulers } from 'rxjs-marbles/jasmine/angular';
import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';

import { GamesEffects } from './games.effect';
import { GameGalleryService } from '../../components/game-gallery/game-gallery.service';
import { GameGalleryComponent } from '../../components/game-gallery/game-gallery.component';
import { GamingRoute } from '../../routes';
import { Game, GamesState } from './games.state';
import { ApiService } from '../../providers/api.service';
import { gamesReducers } from './games.reducer';
import { AppModule } from '../../app.module';
import { selectorGamesGetAll } from './games.selector';
import { Ribbon } from '../../components/game-gallery/ribbon';

export function mockSelector<TStoreState, TValue>(
  store: MockStore<TStoreState>,
  selector: MemoizedSelector<TStoreState, TValue> | MemoizedSelectorWithProps<TStoreState, any, TValue>,
  initialValue: TValue,
  selectorValues: Observable<TValue>
) {
  store.overrideSelector(selector, initialValue);

  return selectorValues.pipe(
    tap(v => {
      selector.setResult(v);
      store.refreshState();
    })
  ).subscribe();
}

describe('GameGalleryService', () => {
  let actions$: Observable<any>;
  let effects: GamesEffects;
  let service: GameGalleryService;
  let store: MockStore<GamesState>;
  const gameMock: Game = {
    id: 'NETHEWISHMASTER',
    categories: [
      'top',
      'slots',
      'new'
    ],
    name: 'The Wish Master',
    image: '//stage.whgstage.com/scontent/images/games/NETHEWISHMASTER.jpg'
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterModule.forRoot([]),
        StoreModule.forRoot({}),
        EffectsModule.forRoot([GamesEffects]),
        EffectsModule.forRoot([GamesEffects]),
        StoreModule.forFeature('games', gamesReducers),
        AppModule
      ],
      providers: [
        provideMockActions(() => actions$),
        provideMockStore({}),
        ApiService,
        GamesEffects,
        GameGalleryService
      ]
    });

    effects = TestBed.get(GamesEffects);
    service = TestBed.get(GameGalleryService);
    store = TestBed.get(Store) as MockStore<GamesState>;

  });


  it('Should getGamesStream()', fakeSchedulers(() => {
    let received: Game[];
    const currentRoute: GamingRoute = {
      path: 'top-games',
      component: GameGalleryComponent,
      data: {
        label: 'Top games',
        categories: ['top']
      }
    };

    mockSelector(store, selectorGamesGetAll, [], of([gameMock]));

    service.getGamesStream(of(currentRoute)).subscribe(value => (received = value));
    tick(1000);
    expect(received.length).toBeGreaterThan(0);
  }));

  it('Should getRibbon()', () => {
    const currentRoute: GamingRoute = {
      path: 'new-games',
      component: GameGalleryComponent,
      data: {
        label: 'New games',
        categories: ['new']
      }
    };

    const ribbon: Ribbon = service.getRibbon(gameMock, currentRoute, ['top']);
    expect(ribbon.label).toBe('TOP');
  });

});
