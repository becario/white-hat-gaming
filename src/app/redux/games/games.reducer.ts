import { Action, createReducer, on } from '@ngrx/store';

import { gamesAdapter, GamesState } from './games.state';
import * as GamesActions from './games.action';

export const initialState: GamesState = gamesAdapter.getInitialState(null);
const reducer = createReducer(
  initialState,
  on(GamesActions.actionGamesGetItemsSuccess, (state, payload) => {
    return gamesAdapter.upsertMany(payload.games, state);
  })
);

export function gamesReducers(state: GamesState | undefined, action: Action) {
  return reducer(state, action);
}
