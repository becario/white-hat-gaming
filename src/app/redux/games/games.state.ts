import {
  createEntityAdapter,
  EntityAdapter,
  EntityState
} from '@ngrx/entity';
import { createFeatureSelector } from '@ngrx/store';
import { Observable } from 'rxjs';

export interface GamesState extends EntityState<Game> {}
export const gamesAdapter: EntityAdapter<Game> = createEntityAdapter<Game>();
export interface Game {
  id: string,
  categories: string[],
  name: string,
  image: string,
  timer: Observable<boolean>
}
export const getGamesModuleState = createFeatureSelector<GamesState>(
  'games'
);
