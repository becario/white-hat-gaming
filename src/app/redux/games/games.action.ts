import { createAction, props } from '@ngrx/store';
import { Game } from './games.state';

export const actionGamesGetItems = createAction(
  '[GAMES] Get items',
);

export const actionGamesGetItemsSuccess = createAction(
  '[GAMES] Get items success',
  props<{ games: Game[] }>()
);

export const actionGamesGetItemsFail = createAction(
  '[GAMES] Get items fail',
  props<{ error: string }>()
);
