import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, delay, map, switchMap, tap } from 'rxjs/operators';
import { of } from 'rxjs';

import { ApiService } from '../../providers/api.service';
import { Game } from './games.state';
import * as GamesActions from './games.action';
import * as RoutingActions from '../routing/routing.action';
import { Store } from '@ngrx/store';

@Injectable()
export class GamesEffects {

  constructor(
    private actions$: Actions,
    private apiService: ApiService,
    private store: Store<any>
  ) {
  }

  actionGamesGetItems = createEffect(() => this.actions$.pipe(
    ofType(GamesActions.actionGamesGetItems),
    switchMap(() => {
      return this.apiService.getGames().pipe(
        map((games: Game[]) => GamesActions.actionGamesGetItemsSuccess({ games })),
        catchError((err) => of(GamesActions.actionGamesGetItemsFail(err)))
      );
    })
  ));

  actionGamesGetItemsFail = createEffect(() => this.actions$.pipe(
    ofType(GamesActions.actionGamesGetItemsFail),
    tap((action) => {
      console.error(action);
    })
  ), { dispatch: false });

  actionGamesGetItemsSuccess = createEffect(() => this.actions$.pipe(
    ofType(GamesActions.actionGamesGetItemsSuccess),
    delay(1000),
    map(() => {
      setTimeout(() => {
        this.store.dispatch(RoutingActions.actionChangeLoadingStatus({ value: null }))
      }, 250);
      return RoutingActions.actionChangeLoadingStatus({ value: false });
    })
  ))

}
