import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, map, switchMap, tap } from 'rxjs/operators';
import { of } from 'rxjs';

import { ApiService } from '../../providers/api.service';
import { Jackpot } from './jackpots.state';
import * as JackpotsActions from './jackpots.action';

@Injectable()
export class JackpotsEffects {

  constructor(
    private actions$: Actions,
    private apiService: ApiService
  ) {
  }

  actionJackpotsGetItems = createEffect(() => this.actions$.pipe(
    ofType(JackpotsActions.actionJackpotsGetItems),
    switchMap(() => {
      return this.apiService.getJackpots().pipe(
        map((jackpots: Jackpot[]) => {
          return JackpotsActions.actionJackpotsGetItemsSuccess({ jackpots });
        }),
        catchError((err) => of(JackpotsActions.actionJackpotsGetItemsFail(err)))
      );
    })
  ));

  actionJackpotsGetItemsFail = createEffect(() => this.actions$.pipe(
    ofType(JackpotsActions.actionJackpotsGetItemsFail),
    tap((action) => {
      console.error(action);
    })
  ), { dispatch: false });

}
