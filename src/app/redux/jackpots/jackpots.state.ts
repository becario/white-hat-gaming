import {
  createEntityAdapter,
  EntityAdapter,
  EntityState
} from '@ngrx/entity';
import { createFeatureSelector } from '@ngrx/store';
import { Observable } from 'rxjs';

export interface JackpotsState extends EntityState<Jackpot> {}
export const jackpotsAdapter: EntityAdapter<Jackpot> = createEntityAdapter<Jackpot>();
export interface Jackpot {
  id: string,
  amount: number,
  currency: string
  timer?: Observable<boolean>
}
export const getJackpotsModuleState = createFeatureSelector<JackpotsState>(
  'jackpots'
);
