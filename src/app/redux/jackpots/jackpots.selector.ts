import { createSelector } from '@ngrx/store';
import { getJackpotsModuleState } from './jackpots.state';

export const selectorJackpotsGetAll = createSelector(
  getJackpotsModuleState,
  state => {
    const ids: any[] = state.ids;
    const jackpots = ids.map((id) => state.entities[id]);

    return jackpots;
  }
);
