import { Action, createReducer, on } from '@ngrx/store';

import { jackpotsAdapter, JackpotsState } from './jackpots.state';
import * as JackpotsActions from './jackpots.action';

export const initialState: JackpotsState = jackpotsAdapter.getInitialState(null);
const reducer = createReducer(
  initialState,
  on(JackpotsActions.actionJackpotsGetItemsSuccess, (state, payload) => {
    return jackpotsAdapter.upsertMany(payload.jackpots, state);
  })
);

export function jackpotsReducers(state: JackpotsState | undefined, action: Action) {
  return reducer(state, action);
}
