import { createAction, props } from '@ngrx/store';
import { Jackpot } from './jackpots.state';

export const actionJackpotsGetItems = createAction(
  '[JACKPOTS] Get items',
);

export const actionJackpotsGetItemsSuccess = createAction(
  '[JACKPOTS] Get items success',
  props<{ jackpots: Jackpot[] }>()
);

export const actionJackpotsGetItemsFail = createAction(
  '[JACKPOTS] Get items fail',
  props<{ error: string }>()
);
