import { BrowserModule, HAMMER_GESTURE_CONFIG, HammerGestureConfig } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import 'hammerjs';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ApiService } from './providers/api.service';
import { GamesEffects } from './redux/games/games.effect';
import { JackpotsEffects } from './redux/jackpots/jackpots.effects';
import { routingReducers } from './redux/routing/routing.reducer';
import { gamesReducers } from './redux/games/games.reducer';
import { jackpotsReducers } from './redux/jackpots/jackpots.reducer';
import { GameGalleryModule } from './components/game-gallery/game-gallery.module';
import { MenuModule } from './components/menu/menu.module';
import { environment } from '../environments/environment';

export class CustomHammerConfig extends HammerGestureConfig  {
  overrides = <any>{
    'pinch': { enable: false },
    'rotate': { enable: false }
  }
}

let devTools = [StoreDevtoolsModule.instrument({
  maxAge: 25,
  logOnly: false
})];
if (environment.production) {
  devTools = [];
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    StoreModule.forRoot({}),
    EffectsModule.forRoot([GamesEffects, JackpotsEffects]),
    StoreModule.forFeature('routing', routingReducers),
    StoreModule.forFeature('games', gamesReducers),
    StoreModule.forFeature('jackpots', jackpotsReducers),
    ...devTools,
    HttpClientModule,
    MenuModule,
    GameGalleryModule
  ],
  providers: [
    ApiService,
    {
      provide: HAMMER_GESTURE_CONFIG,
      useClass: CustomHammerConfig
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
