import {
  animate, animation,
  group,
  query,
  style,
  transition,
  trigger, useAnimation
} from '@angular/animations';
import { menuTransitions } from './menu-transitions';

const slideToRight = animation([
  query(':enter, :leave', [
    style({
      position: 'absolute',
      right: 0,
      width: '100%'
    })
  ], { optional: false }),
  query(':enter', [
    style({ right: '-100%'})
  ]),
  group([
    query(':leave', [
      animate('500ms ease', style({ right: '100%'}))
    ], { optional: true }),
    query(':enter', [
      animate('500ms ease', style({ right: '0%'}))
    ])
  ])
]);

const slideToLeft = animation([
  query(':enter, :leave', [
    style({
      position: 'absolute',
      right: 0,
      width: '100%'
    })
  ], { optional: true }),
  query(':enter', [
    style({ left: '-100%'})
  ]),
  group([
    query(':leave', [
      animate('500ms ease', style({ left: '100%'}))
    ], { optional: true }),
    query(':enter', [
      animate('500ms ease', style({ left: '0%'}))
    ])
  ])
]);

//// This function calculates all the possible combinations of navigation for the different menu options.
//// This cannot be compiled under AOT mode (for production). Instead static file menu-transitions.ts will be used.
//// More info: https://github.com/ng-packagr/ng-packagr/issues/727

/*const getTransitions = (optionCount: number): any[] => {
  const transitions = [];
  for (let pageIndex = 0; pageIndex < optionCount; pageIndex ++) {
    for (let pageIndexEnd = 0; pageIndexEnd < optionCount; pageIndexEnd ++) {
      if (pageIndex !== pageIndexEnd) {
        const anim = pageIndex < pageIndexEnd ? [useAnimation(slideToRight)] : [useAnimation(slideToLeft)];
        transitions.push(
          transition(`page${pageIndex} => page${pageIndexEnd}`, anim)
        );
      }
    }
  }

  return transitions;
};
export const slider = trigger('routeAnimations', getTransitions(10)); */
export const slider = trigger('routeAnimations', menuTransitions);

