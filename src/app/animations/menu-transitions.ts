export const menuTransitions = [
  {
    "type": 1,
    "expr": "page0 => page1",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": false
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "right": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page0 => page2",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": false
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "right": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page0 => page3",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": false
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "right": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page0 => page4",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": false
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "right": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page0 => page5",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": false
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "right": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page0 => page6",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": false
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "right": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page0 => page7",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": false
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "right": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page0 => page8",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": false
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "right": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page0 => page9",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": false
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "right": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page1 => page0",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": true
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "left": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page1 => page2",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": false
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "right": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page1 => page3",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": false
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "right": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page1 => page4",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": false
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "right": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page1 => page5",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": false
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "right": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page1 => page6",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": false
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "right": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page1 => page7",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": false
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "right": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page1 => page8",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": false
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "right": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page1 => page9",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": false
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "right": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page2 => page0",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": true
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "left": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page2 => page1",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": true
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "left": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page2 => page3",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": false
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "right": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page2 => page4",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": false
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "right": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page2 => page5",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": false
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "right": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page2 => page6",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": false
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "right": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page2 => page7",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": false
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "right": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page2 => page8",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": false
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "right": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page2 => page9",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": false
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "right": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page3 => page0",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": true
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "left": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page3 => page1",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": true
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "left": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page3 => page2",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": true
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "left": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page3 => page4",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": false
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "right": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page3 => page5",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": false
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "right": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page3 => page6",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": false
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "right": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page3 => page7",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": false
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "right": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page3 => page8",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": false
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "right": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page3 => page9",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": false
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "right": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page4 => page0",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": true
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "left": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page4 => page1",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": true
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "left": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page4 => page2",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": true
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "left": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page4 => page3",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": true
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "left": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page4 => page5",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": false
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "right": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page4 => page6",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": false
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "right": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page4 => page7",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": false
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "right": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page4 => page8",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": false
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "right": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page4 => page9",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": false
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "right": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page5 => page0",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": true
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "left": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page5 => page1",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": true
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "left": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page5 => page2",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": true
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "left": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page5 => page3",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": true
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "left": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page5 => page4",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": true
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "left": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page5 => page6",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": false
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "right": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page5 => page7",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": false
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "right": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page5 => page8",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": false
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "right": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page5 => page9",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": false
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "right": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page6 => page0",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": true
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "left": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page6 => page1",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": true
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "left": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page6 => page2",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": true
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "left": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page6 => page3",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": true
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "left": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page6 => page4",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": true
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "left": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page6 => page5",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": true
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "left": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page6 => page7",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": false
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "right": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page6 => page8",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": false
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "right": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page6 => page9",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": false
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "right": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page7 => page0",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": true
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "left": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page7 => page1",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": true
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "left": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page7 => page2",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": true
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "left": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page7 => page3",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": true
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "left": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page7 => page4",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": true
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "left": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page7 => page5",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": true
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "left": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page7 => page6",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": true
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "left": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page7 => page8",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": false
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "right": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page7 => page9",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": false
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "right": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page8 => page0",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": true
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "left": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page8 => page1",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": true
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "left": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page8 => page2",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": true
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "left": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page8 => page3",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": true
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "left": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page8 => page4",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": true
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "left": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page8 => page5",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": true
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "left": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page8 => page6",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": true
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "left": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page8 => page7",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": true
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "left": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page8 => page9",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": false
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "right": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "right": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page9 => page0",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": true
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "left": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page9 => page1",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": true
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "left": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page9 => page2",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": true
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "left": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page9 => page3",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": true
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "left": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page9 => page4",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": true
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "left": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page9 => page5",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": true
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "left": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page9 => page6",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": true
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "left": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page9 => page7",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": true
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "left": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  },
  {
    "type": 1,
    "expr": "page9 => page8",
    "animation": [
      {
        "type": 10,
        "animation": {
          "type": 8,
          "animation": [
            {
              "type": 11,
              "selector": ":enter, :leave",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "position": "absolute",
                    "right": 0,
                    "width": "100%"
                  },
                  "offset": null
                }
              ],
              "options": {
                "optional": true
              }
            },
            {
              "type": 11,
              "selector": ":enter",
              "animation": [
                {
                  "type": 6,
                  "styles": {
                    "left": "-100%"
                  },
                  "offset": null
                }
              ],
              "options": null
            },
            {
              "type": 3,
              "steps": [
                {
                  "type": 11,
                  "selector": ":leave",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "100%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": {
                    "optional": true
                  }
                },
                {
                  "type": 11,
                  "selector": ":enter",
                  "animation": [
                    {
                      "type": 4,
                      "styles": {
                        "type": 6,
                        "styles": {
                          "left": "0%"
                        },
                        "offset": null
                      },
                      "timings": "500ms ease"
                    }
                  ],
                  "options": null
                }
              ],
              "options": null
            }
          ],
          "options": null
        },
        "options": null
      }
    ],
    "options": null
  }
]
