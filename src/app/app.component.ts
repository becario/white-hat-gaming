import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd, RouterOutlet } from '@angular/router';
import { Store } from '@ngrx/store';
import { filter } from 'rxjs/operators';

import * as GamesActions from './redux/games/games.action';
import * as JackpotsActions from './redux/jackpots/jackpots.action';
import { actionRoutingAddPath } from './redux/routing/routing.action';
import { optionRoutes } from './routes';
import * as pageSlide from './animations/page-slide';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [pageSlide.slider]
})
export class AppComponent implements OnInit {

  constructor(
    private store: Store<any>,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.loadAppData();
    this.routeChangesListener();
  }

  public prepareRoute(outlet: RouterOutlet): string {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData['animation'];
  }

  private loadAppData(): void {
    const jackpotRefreshInterval = 3000;

    this.store.dispatch(GamesActions.actionGamesGetItems());
    this.store.dispatch(JackpotsActions.actionJackpotsGetItems());
    setInterval(() => {
      this.store.dispatch(JackpotsActions.actionJackpotsGetItems());
    }, jackpotRefreshInterval)
  }

  private routeChangesListener(): void {
    this.router.events.pipe(
      filter(event => event instanceof NavigationEnd)
    ).subscribe((event: any) => {
      const path = event.url.substring(1) || event.urlAfterRedirects.substring(1);
      const newRoute = optionRoutes.find((route) => route.path === path);

      if (newRoute) {
        this.store.dispatch(actionRoutingAddPath({ newRoute }));
      }
    });
  }

}
