import { Component, OnInit } from '@angular/core';
import { Route, Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { GamingRoute, optionRoutes } from '../../routes';
import { selectorRoutingGetCurrentRoute } from '../../redux/routing/routing.selector';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  public options: string[];
  public optionRoutes: GamingRoute[];
  public activeRoute$: Observable<Route>;
  public menuUnfolded: boolean;

  constructor(
    private router: Router,
    private store: Store<any>
  ) {
    this.optionRoutes = optionRoutes;
    this.menuUnfolded = false;
  }

  ngOnInit(): void {
    this.activeRoute$ = this.store.pipe(select(selectorRoutingGetCurrentRoute));
  }

  public goToUrl(event: MouseEvent, path: string): void {
    event.preventDefault();
    this.router.navigateByUrl(path).then(() => {
      this.hideMenu();
    });
  }

  public toggleMenu(): void {
    this.menuUnfolded = !this.menuUnfolded;
  }

  public hideMenu(): void {
    this.menuUnfolded = false;
  }

}
