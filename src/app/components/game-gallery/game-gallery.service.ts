import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { select, Store } from '@ngrx/store';
import { delay, filter, map, startWith, withLatestFrom } from 'rxjs/operators';

import { Game, GamesState } from '../../redux/games/games.state';
import { GamingRoute } from '../../routes';
import { selectorGamesGetAll } from '../../redux/games/games.selector';
import {
  selectorRoutingGetAvailableRoutes,
  selectorRoutingGetCurrentRoute,
  selectorRoutingGetLoadingStatus
} from '../../redux/routing/routing.selector';
import { Ribbon } from './ribbon';
import { Jackpot } from '../../redux/jackpots/jackpots.state';
import { selectorJackpotsGetAll } from '../../redux/jackpots/jackpots.selector';
import { SiblingRoutes } from '../../redux/routing/routing.state';

@Injectable()
export class GameGalleryService {

  constructor(private store: Store<GamesState>) {}

  public getSiblingRoutes(currentPath: string): Observable<SiblingRoutes> {
    return this.store.pipe(
      select(selectorRoutingGetAvailableRoutes),
      map((routes: GamingRoute[]) => {
        const currentRouteIndex = routes.findIndex((route) => '/' + route.path === currentPath);
        return {
          prev: routes[currentRouteIndex - 1] || routes[routes.length -1],
          next: routes[currentRouteIndex + 1] || routes[0]
        }
      })
    );
  }

  public getGamesStream(activeRoute$: Observable<GamingRoute>): Observable<Game[]> {
    return this.store.pipe(
      select(selectorGamesGetAll),
      withLatestFrom(activeRoute$),
      map(([games, activeRoute]: [Game[], GamingRoute]) => {
        const currentCategories = activeRoute.data.categories;
        if (currentCategories) {
          const sectionGames = games.filter((game) => {
            for (const currentCategory of currentCategories) {
              return game.categories.includes(currentCategory);
            }
            return false;
          }).map((game, gameIndex) => {
            game.timer = of(true).pipe(
              startWith(false),
              delay(80 * gameIndex)
            );
            return game;
          });
          return sectionGames;
        } else {
          return [];
        }
      })
    );
  }

  public getActiveRouteStream(): Observable<GamingRoute> {
    return this.store.pipe(
      select(selectorRoutingGetCurrentRoute),
      filter((route) => !!route)
    );
  }

  public getJackpotsStream(games$: Observable<Game[]>): Observable<Jackpot[]> {
    return this.store.pipe(select(selectorJackpotsGetAll)).pipe(
      withLatestFrom(games$),
      map(([jackpots, games]: [Jackpot[], Game[]]) => {
        let jackpotsAssigned = 0;
        return jackpots.reverse().map((jackpot) => {
          for (let x = 0; x < games.length; x ++) {
            if (games[x].id === jackpot.id) {
              jackpotsAssigned ++;
              jackpot.timer = of(true).pipe(
                startWith(false),
                delay(80 * jackpotsAssigned)
              );
            }
          }
          return jackpot;
        });
      })
    )
  }

  public getLoadingState(): Observable<boolean> {
    return this.store.pipe(select(selectorRoutingGetLoadingStatus));
  }

  public getRibbon(
    game: Game,
    activeRoute: GamingRoute,
    featuredCategories: string[]
  ): Ribbon {
    if (activeRoute) {
      const currentSectionCategory = activeRoute.data.categories[0];

      for (const gameCategory of game.categories) {
        if (featuredCategories.includes(gameCategory)) {
          if (gameCategory !== currentSectionCategory) {
            switch (gameCategory) {
              case 'new':
                return {
                  image: 'assets/images/ribbon-green.png',
                  label: 'NEW',
                  class: 'green'
                };
              case 'top':
                return {
                  image: 'assets/images/ribbon-white.png',
                  label: 'TOP',
                  class: 'white'
                };
            }
          }
        }
      }
    }

    return null;
  }
}
