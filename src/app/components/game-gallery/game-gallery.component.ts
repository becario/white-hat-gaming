import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';

import { Game } from '../../redux/games/games.state';
import { GameGalleryService } from './game-gallery.service';
import { GamingRoute } from '../../routes';
import { Jackpot } from '../../redux/jackpots/jackpots.state';
import { take } from 'rxjs/operators';
import { SiblingRoutes } from '../../redux/routing/routing.state';

@Component({
  selector: 'app-game-gallery',
  templateUrl: './game-gallery.component.html',
  styleUrls: ['./game-gallery.component.scss']
})
export class GameGalleryComponent implements OnInit {

  public games$: Observable<Game[]>;
  public featuredCategories: string[];
  public hoverGameId: string;
  public activeRoute$: Observable<GamingRoute>;
  public newJackpot: boolean;
  public jackpots: Jackpot[];
  public loading: boolean;
  public loading$: Observable<boolean>;

  private jackpots$: Observable<Jackpot[]>;
  private jackpotsSubscription: Subscription;
  private siblingRoutes: SiblingRoutes;

  constructor(
    public gameGalleryService: GameGalleryService,
    private router: Router
  ) {
    this.featuredCategories = ['new', 'top'];
    this.newJackpot = true;
    this.loading = true;
  }

  ngOnInit(): void {
    this.activeRoute$ = this.gameGalleryService.getActiveRouteStream();
    this.games$ = this.gameGalleryService.getGamesStream(this.activeRoute$);
    this.jackpots$ = this.gameGalleryService.getJackpotsStream(this.games$);
    this.loading$ = this.gameGalleryService.getLoadingState();

    this.jackpotsSubscription = this.jackpots$.subscribe(
      (jackpots: Jackpot[]) => {
        this.newJackpot = false;
        this.jackpots = jackpots;

        setTimeout(() => this.newJackpot = true);
      }
    );

    this.gameGalleryService.getSiblingRoutes(this.router.url).pipe(take(1)).subscribe(
      (siblingRoutes: SiblingRoutes) => {
        this.siblingRoutes = siblingRoutes;
      }
    )
  }

  ngOnDestroy(): void {
    this.jackpotsSubscription.unsubscribe();
  }

  public mouseEnter(gameId: string): void {
    this.hoverGameId = gameId;
  }

  public gameHasJackpot(gameId: string, jackpots: Jackpot[]): Jackpot {
    return jackpots.find((jackpot) => jackpot.id === gameId);
  }

  public onSwipeLeft() {
    this.router.navigateByUrl(this.siblingRoutes.next.path);
  }

  public onSwipeRight() {
    this.router.navigateByUrl(this.siblingRoutes.prev.path);
  }

}

