import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GameGalleryComponent } from './game-gallery.component';
import { GameGalleryService } from './game-gallery.service';

@NgModule({
  declarations: [GameGalleryComponent],
  imports: [CommonModule],
  providers: [GameGalleryService],
  exports: [GameGalleryComponent]
})
export class GameGalleryModule {}
