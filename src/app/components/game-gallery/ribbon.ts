export interface Ribbon {
  image: string;
  label: string;
  class: string;
}
